# Process Analytics Docker Containers

This module contains the necessary files to build and run Docker containers for our application. Follow the instructions below to get started.

## Prerequisites

1. Ensure that [Docker](https://www.docker.com/) is installed on your system. If not, you can download and install it from the official Docker website.

2. Ensure that you have permissions to run Docker commands. You might need `sudo` permissions depending on your setup.

## Getting Started

### 1. Environment Setup

Before building and running the containers, make sure to set up your environment variables:

```bash
cp .env.sample .env
```

Edit the `.env` file as necessary to match your configuration or requirements.

### 2. Building and Running Containers

To build and run the containers, execute the provided script:

```bash
./run_containers
```

If you encounter any permission issues, you might need to give execute permissions to the script:

```bash
chmod +x run_containers
```

Then, try running the script again.

## Troubleshooting

- Ensure that Docker is running on your system. If it's not, start the Docker service.
- If you face issues related to Docker permissions, consider adding your user to the `docker` group or using `sudo` before Docker commands.
- Ensure that the `.env` file has the correct values set for your environment.

## Support

If you encounter any issues or need further assistance, feel free to reach out to the module maintainers or consult the Docker official documentation.

---

Remember to adapt this template to your specific needs and add any additional information that might be relevant to your project.