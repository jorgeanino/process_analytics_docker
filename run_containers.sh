#!/bin/bash

# Detiene y elimina los contenedores existentes
docker-compose down

# Construye las imágenes de Docker
docker-compose build --no-cache

# Inicia los contenedores en modo detached (en segundo plano)
docker-compose up -d
